
SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `devices`
-- ----------------------------
DROP TABLE IF EXISTS `Devices`;
CREATE TABLE `Devices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `deviceName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of devices
-- ----------------------------
INSERT INTO `Devices` VALUES ('22', 'Device 22');

-- ----------------------------a
-- Table structure for `readings`
-- ----------------------------
DROP TABLE IF EXISTS `Readings`;
CREATE TABLE `Readings` (
  `device_id` int(11) NOT NULL DEFAULT '22',
  `reading_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `reading_value` decimal(10,1) DEFAULT NULL,
  PRIMARY KEY (`device_id`,`reading_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of readings
-- ----------------------------
