<?php

class CsvParseCommand extends CConsoleCommand {

    public function actionImport($filename){

        $interval = new DateInterval( "PT30M" );
        $device=22;

        $info = pathinfo($filename);
        if ($info["extension"] != "csv") {
            echo 'Podano bledne zrodlo danych. Podaj dostep do pliku .csv';
            return false;
        }

        if(copy ( $filename , dirname(Yii::app()->getBasePath()).'/uploads/'.$info["basename"])){
            $filename=dirname(Yii::app()->getBasePath()).'/uploads/'.$info["basename"];
        } else {
            echo 'Upload pliku się nie powiódł.';
            return false;
        };

        $i=0;
        $handle=fopen($filename, "r");
        if ($handle){
            while(($line = fgetcsv($handle)) !== false) {
                $i++;
                if ($i>1) {
                    $readtime= DateTime::createFromFormat('d/m/Y g:i:s',$line[1].'00:00:00');
                    for($j=2;$j<50;$j++){

                        $reading = Reading::model()->findByPk(array('reading_time' => $readtime->format('Y-m-d H:i:s'), 'device_id' => $device));
                        if(!$reading) $reading= new Reading();

                        $reading->device_id=$device;
                        $reading->reading_time=$readtime->format('Y-m-d H:i:s');
                        $reading->reading_value=$line[$j];
                        $readtime->add($interval);
                        $reading->save();
                    }
                }
                echo 'Wczytano linie '.($i-1)."\r\n";
            }
            echo "Import zakonczony \r\n";
            fclose($handle);
            //unlink($filename);
        }

    }
} 