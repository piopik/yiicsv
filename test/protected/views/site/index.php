<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>

<h1><i><?php echo CHtml::encode(Yii::app()->name); ?></i></h1>
<ul>

    <li><p>W protected/commands znajduje się klasa CsvParseCommand dziedzicząca po CConsoleCommand. Akcja import uploaduje, parsuje, i zapisuje w bazie wyniki z pliku CSV.
    <br>Uruchamiana poleceniem:<br><br>
yiic csvparse import --filename=[url do pliku] <br><br>
przykładowo:<br><br>
    yiic csvparse import --filename=http://piopix.pl/test.csv</p></li>

    <li><p>W protected/data jest plik schema.mysql.sql ze schematem bazy mysql</p></li>

    <li><p>Pod adresem index.php/site/graph mamy wykres miesięczny. Bez parametrów url wyświetla się najświeższy wprowadzony miesiąc.
Po dodaniu do urla paramaetrów /[rok]/[miesiac] otrzymujemy wykres dla danego miesiąca.
Url może przyjmować również parametr device (?device=[id_device]). Domyślnie przyjmuje 22.
Przykładowy adres dla wykresu z marca 2000 pomiarów urządzenia o id 12 to:<br><br>

index.php/site/graph/2000/03?device=12
</p></li>

</ul>