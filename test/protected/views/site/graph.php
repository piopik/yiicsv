
<script>

    function hc(data, month, year){

        $('#yw0').highcharts({
            title: {
                text: year+' '+month
            },
            series: [{
                data: data

            }]
        })

//        Ajaxowa czesc potrzebuje przerobienia przechowywania informacji o aktualnie wybranym miesiacu i roku z phpowych zmiennych na zmienne js i dopisania ich zmiany w funkcji do wyswietlania wykresu hc. W tej chwili mozna sie cofnac o jeden miesiac.

    }
</script>
<?php

$y =array_map(create_function('$value', 'return (int)$value;'),$y);


$this->Widget('ext.highcharts.HighchartsWidget', array(
    'options'=>array(

        'chart' => array('zoomType' => 'x'),
        'title' => array('text' => $month.' '.$year),
        'yAxis' => array(
            'title' => array('text' => 'Wartość'),

        ),
        'xAxis' => array(
            'type' => 'datetime',
        ),
        'series' => array(
            array('data' => $y, 'pointInterval' => 1000*60 *30, 'pointStart' => mktime(0, 0, 0, $month, 1, $year)*1000),
        )
    )
));

if($month==12){
    $prev=$year.'/11';
    $prev_y=$year;
    $prev_m=$month-1;
    $next=($year+1).'/1';
    $next_y=$year+1;
    $next_m=1;
} elseif ($month==1){
    $prev=($year-1).'/12';
    $prev_y=$year-1;
    $prev_m=12;
    $next_y=$year;
    $next_m=$month+1;
    $next=$year.'/2';
} else {
    $prev=$year.'/'.($month-1);
    $prev_y=$year;
    $prev_m=$month-1;
    $next=$year.'/'.($month+1);
    $next_y=$year;
    $next_m=$month+1;
};

echo CHtml::ajaxLink(
    "Poprzedni miesiac",
    Yii::app()->createUrl( 'site/graphajax' ),
    array( // ajaxOptions
        'type' => 'POST',
    'beforeSend' => "function( request )
                     {

                     }",
    'success' => "function( data )
                  {
                    hc(data['y'],data['year'],data['month']);
                  }",
    'data' => array( 'month' => $prev_m, 'year' => $prev_y )
  ),
  array( //htmlOptions
      'href' => Yii::app()->createUrl( 'site/graph' )
  )
);



?>



<!--<a href="--><?php //echo Yii::app()->homeUrl;?><!--/site/graph/--><?php //echo $prev;?><!--">POPRZEDNI MIESIĄC</a>-->
<!---->
<!--<a href="--><?php //echo Yii::app()->homeUrl;?><!--/site/graph/--><?php //echo $next;?><!--">NASTĘPNY MIESIĄC</a>-->
<!---->
<!--<a href="--><?php //echo Yii::app()->homeUrl;?><!--/site/graph/">NAJNOWSZY MIESIĄC</a>-->

