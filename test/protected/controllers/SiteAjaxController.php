<?php

date_default_timezone_set('Europe/Belgrade');

class SiteAjaxController extends Controller
{

	public function actionIndex()
	{
		$this->render('index');
	}

	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

    public function actionGraph($year=0,$month=0,$device=22)
    {
        if(!Device::model()->findAllByPk($device)){
            $this->render('wrongDevice');
            return 0;
        }


        if($year==0 && $month==0){
            $startDate=DateTime::createFromFormat('Y-m-d H:i:s', Reading::model()->findBySql('select * from Readings order by reading_time desc limit 1;')->reading_time);
            if(!$startDate){
                //baza pusta
                return 0;
            }

            $year=$startDate->format('Y');
            $month=$startDate->format('m');
        }

        $readings = Reading::model()->findAllBySql('select * from Readings where device_id='.$device.' AND MONTH(reading_time)='.$month.' AND YEAR(reading_time)='.$year.' order by reading_time asc ;');

        $x = Array();
        $y = Array();

        foreach($readings as $r){
            array_push($x, $r->reading_time);
            array_push($y, $r->reading_value);
        }

        $this->render('graph',
            array(
                'x' => $x,
                'y' => $y,
                'month' => $month,
                'year' => $year,
            )
        );
    }

}