<?php

class Reading extends CActiveRecord{
    public $reading_time_cet= null;

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'Readings';
    }

    public function rules()
    {
        return array(
            array('device_id, reading_time, reading_value', 'required'),
        );
    }

    public function relations()
    {
        return array(
            'device_id' => array(self::HAS_ONE, 'Devices','id'),
        );
    }

    public function latest(){

    }

} 