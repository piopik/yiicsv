<?php

class Device extends CActiveRecord{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'Devices';
    }

    public function rules()
    {
        return array(
            array('id', 'required'),
        );
    }

    public function relations()
    {
        return array(
            'device_id' => array(self::HAS_MANY, 'Readings','device_id'),
        );
    }

} 